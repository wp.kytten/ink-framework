<?php if ( ! defined( 'ABSPATH' ) ) {
	exit( '[Ink Framework] Direct access is not allowed' );
}

/*
 * Bounce if already loaded
 */
if ( defined( 'INK_FRAMEWORK' ) ) {
	return;
}

define( 'INK_FRAMEWORK', true );


/*
 * Skip loading when installing.
 */
if ( defined( 'WP_INSTALLING' ) && WP_INSTALLING ) {
	return;
}

if ( isset( $_REQUEST['action'] ) && ( 'heartbeat' == strtolower( $_REQUEST['action'] ) ) ) {
	return;
}

if ( ! function_exists( 'inkNotifyPhpVersion' ) ) {
	/**
	 * Notify the client if PHP version is not supported and deactivate the plugin
	 */
	function inkNotifyPhpVersion()
	{
		?>
		<div class="notice notice-error">
			<p><?php echo sprintf( __( '<strong>%s</strong> plugin needs PHP version >= 5.4.0 in order to function properly. Please contact your hosting support and ask them to update your PHP version to at least <strong>5.4.0</strong>.', 'ink-fw' ), 'Ink Framework' ); ?></p>
		</div>
		<?php
		deactivate_plugins( 'ink-framework/index.php' );
		unset( $_GET['activate'], $_GET['plugin_status'], $_GET['activate-multi'] );
		return;
	}
}

//#! If PHP < v5.4.0, notify and disable the plugin
if ( version_compare( phpversion(), '5.4.0', '<' ) ) {
	add_action( "network_admin_notices", 'inkNotifyPhpVersion', 0 );
	add_action( "admin_notices", 'inkNotifyPhpVersion', 0 );
}


/*
 * Autoloader
 */
require_once( dirname( __FILE__ ) . '/autoloader.php' );
$loader = new \Ink\Psr4AutoloaderClass();
$loader->register();
$loader->addNamespace( 'Ink', dirname( __FILE__ ) . '/ink/src' );


/**
 * Instantiate & configure the framework
 */
\Ink\Framework::getInstance( 'ink' )->init( [

	//#! [REQUIRED] Provide the system path to the framework directory
	'ink-dir' => '',
	//#! [REQUIRED] Provide the uri to the framework directory
	'ink-uri' => '',
	//#! [REQUIRED] Provide the path to the WP_CONTENT directory
	'ink-content-dir' => '',

	//#! [OPTIONAL] Configure logging
	'logging' => [
		'enable-logging' => true,
		'instance-name' => 'ink',
		'log-file-name' => 'ink-debug.log',
		'min-logging-level' => \Ink\Helpers\Logger::SYSTEM,
	],
] );

